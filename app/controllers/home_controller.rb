class HomeController < ApplicationController
  def index
  	if params[:review_option] == "no_of_lines"
    	get_data  
    else
  	@list_of_files = [CodeReview.find_where_query(session[:file_path]+'/app/views')] rescue []
    end
  end

  def create
  	tmp = params[:files].tempfile
  	destiny_file = File.join(Rails.root.to_s,'public', 'upload_files', params[:files].original_filename)
  	FileUtils.move tmp.path, destiny_file
  	params[:file_path] = destiny_file.split('.').first
  	session[:file_path] = params[:file_path]

  	CodeReview.extract_zip(destiny_file, File.join(Rails.root.to_s,'public', 'upload_files'), destiny_file)
  	get_data 
  	render :index
  end

  # def extract_zip(file, destination, zip_file)
	 #  FileUtils.mkdir_p(destination) if not File.directory?(destination)
	 #  binding.pry
	 #  Zip::File.open(file) do |zip_file|
	 #    zip_file.each do |f|
	 #      fpath = File.join(destination, f.name)
	 #      zip_file.extract(f, fpath) unless File.exist?(fpath)
	 #    end
	 #  end
	 #  File.delete(zip_file)
  # end

  def get_data
  	if params[:get_dir]
  		root_dir = Rails.root.to_s.split("/") - [""]
  		params[:file_path] = "/"+root_dir[0]+"/"+root_dir[1] if not params[:file_path].present?
  		search = params[:file_path]

  		if search.first != "/"
  			search = "/"+search
  		end
		@get_directories = CodeReview.get_list_dir(search) 
  		
	else
	  	if params[:file_path].present? and params[:review_option] == "no_of_lines"
	  		split_file_path = params[:file_path].split('/')
	  		proj_path = split_file_path.last
	  		split_file_path.delete(proj_path)
	  		split_file_path = split_file_path.join('/')
	  		if params[:sub_dir].present? and params[:sub_dir].include?(split_file_path+"/") 
	  			params[:file_path] = (params[:sub_dir].split('/')-[""]).join("/")+proj_path
	  			params[:sub_dir] = []
	  		end
	  		@file_directories = CodeReview.get_list_dir(params[:file_path])
	  	    @file_directories = abc(@file_directories, hash_val=nil, arr=nil)
	  	# @file_directories = @file_directories.group_by(&:keys).map{|k, v| {k.first => v.flat_map(&:values)}}
	  		if params[:sub_dir].present?
	  			@list_of_files = CodeReview.send_root_and_sub_dir(params[:file_path], params[:sub_dir])
	  		end
	  	else
	  		
	  	end
	end

  end

  def abc(act_hash, hash_val=nil, arr=nil)
    hash_val = act_hash if hash_val.nil?
    hash_val.each do |k, v|
	 	if v and v.count > 1
	 	   act_hash[k] = {}
	 		v.each do |val|
	 		  a = act_hash[val]
	 		  act_hash[k][val] = a if a
	 		  act_hash[k] = abc(act_hash[k], nil, arr=nil) if a and a.count > 0
	 		  # act_hash[k][val] = (c ? c : {})
	 		end
	 	end
	 end

	 return act_hash
  end

end
