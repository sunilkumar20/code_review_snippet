module ApplicationHelper

	def checkbox(file_directories, a = "")
		p a
		file_directories.each do |file_dir, val|
		   a += "<input type='checkbox' name=sub_dir[file_dir][], value=#{file_dir}, #{((params[:sub_dir] and params[:sub_dir].include?(file_dir)) ? true : false)}><br/>"
		   checkbox(val, a) if val.class == Hash
		 end
		 a
	end

	def print(hash_data, a= "")
		hash_data.each do |k, val|
			a += k + " "
			hash_data[k] = print(val, a= "") if val.class == Hash
			a += "<br/>"
		end
		a
	end
end
