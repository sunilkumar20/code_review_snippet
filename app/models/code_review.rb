require 'rubygems'
require 'zip'
class CodeReview

  def self.count_lines_in_method(file_path)
  i = count = end_line = last_end = def_count = 0
  result = {}
  find_def = []
  arr = File.readlines(file_path)
  ignore_words = ["private", "protected", "public"]
  arr.each_with_index do |line, index|
    sent = line.to_s.gsub(" ", "")
    if sent[0] != "#"
      i +=1
      v = sent.gsub(/[\n]/, '')
      end_line += 1 if (sent.length == 1 and sent == "\n") or ignore_words.include?(v)
      if sent == "end\n" and def_count > 0
        last_end = 1
        end_line = 0
      end
      if line.match('def ') and not line.split('def ').first.match('#')
     
         def_count += 1
         i = i - end_line if end_line > 0 or last_end > 0
         # result << find_def[def_count-2].keys.first.gsub(/[\n]/, '').to_s + "   Line Number " + (find_def[def_count-2].values.first).to_s + "  Line Count " + i.to_s if       find_def.length > 0 if i > 20
         
         result[file_path] = [] if not result[file_path].present?
         result[file_path] << {method_name: (find_def[def_count-2].keys.first.gsub(/[\n]/, '')).to_s, line_number: (find_def[def_count-2].values.first).to_s, line_count: i} if find_def.length > 0 and i > 20
         find_def << {line => (index +1)}
         count += 1
         i = 0
         last_end = 0
      end
    end
      if arr.length-1 == index
         i = i- end_line if end_line > 0 or last_end > 0
        # result << find_def[def_count-1].keys.first.gsub(/[\n]/, '').to_s + "   Line Number " + (find_def[def_count-1].values.first).to_s + "  Line Count " + i.to_s if i > 20
        result[file_path] = [] if not result[file_path].present?
        result[file_path] << {method_name: find_def[def_count-1].keys.first.gsub(/[\n]/, '').to_s, line_number: (find_def[def_count-1].values.first).to_s, line_count: i.to_s} if i > 20 and def_count > 0
      end 
  end

  if result.count > 0 
    # puts file_path
    # puts result.join(" \n")
    # puts "\n"
    return result
  else
    return []
  end
     
  end

  def self.find_where_query(file_path)
    result = {}
    data = `grep -r '.where' "#{file_path}/"* -n`
    data += `grep -r '.find' "#{file_path}/"* -n`
    data = data.gsub(/[\n]/, "||||").split("||||")
    data.each do |d|
      url_with_data = d.split('          ')
      split_url = url_with_data.first.split(':')
      file_path = split_url.first
      result[file_path] = [] if not result[file_path].present?
      result[file_path] << {method_name: (split_url.count == 2 ? url_with_data.last : split_url[2.. split_url.length].first).first(100).to_s+"  ...", line_number: split_url.first(2).last.to_s, line_count: nil}
    end
    if result.count > 0 
    # puts file_path
    # puts result.join(" \n")
    # puts "\n"
    return result
  else
    return []
  end
  end


  def self.function_check(root_directory, dirct, an_dir, i, arr = [])
    i += 1
    for item in 0..(dirct.length - 1)
      root_directory = (root_directory.split('/') - ["/"]).join('/')
      an_dir = (an_dir.split('/') - [""]).join('/')
      if dirct[item].split('.').last == "rb"
        arr << CodeReview.count_lines_in_method(root_directory +"/"+ an_dir + "/"+ dirct[item])
      else
       dir_data = Dir.entries(root_directory +"/"+ an_dir + "/" + dirct[item]) rescue nil
       if dir_data and not dir_data.empty?
          dir_data.delete('.') 
          dir_data.delete('..')
          CodeReview.function_check(root_directory + "/" + an_dir + "/", dir_data, dirct[item], i, arr)
        end
      end
    end
  end
 
  def self.send_root_and_sub_dir(root_directory, sub_dir)
    sub = []
    for index in 0..sub_dir.length-1
      if not sub_dir.include?(root_directory.split('/').join('/'))
        data = Dir.entries(root_directory + sub_dir[index].to_s)
        data.delete('.') 
        data.delete('..')
        i = 0
        CodeReview.function_check(root_directory, data, sub_dir[index], i, sub)
      else
        split_root = root_directory.split("/")
        pro_dir = root_directory.split("/").last
         split_root.delete(pro_dir)
        root_dir = split_root.join('/')+"/"
        data = Dir.entries(root_dir + pro_dir)
        data.delete('.') 
        data.delete('..')
        i = 0
        CodeReview.function_check(root_dir, data, pro_dir, i, sub)
      end
    end
    sub.flatten
  end

  def self.get_list_dir(rootdirect, fld = {}, keys= [], dir_path=nil, hash_root = nil)
    # @folder = {} if not @folder.present?
    # rootdirect = main_dir if rootdirect.nil?
      curr_dir = rootdirect.split('/').last
      if Dir.exist?(rootdirect)
        folder = Dir.entries(rootdirect) 
        keys = (rootdirect.split(hash_root).last.split('/') - [""] - ['..'] - ['.'])
        fol = folder.reject{|a| File.file?(a) or a.include?(".") or a.include?("~")}
        folder.select{|s| s if not s.include?('.')}.select{|s| s if not s.include?('~')}.each do |f|
          if not File.file?(f)
          # if f == "app" or f == "controllers" or f == "admin"
            # fld[curr_dir] = {} if not fld.present? #or not fld[curr_dir].present?
            # fld[curr_dir] = {dir_path => {} } if dir_path.present? and not keys.present?
            # fld = keys.reverse.inject(curr_dir) { |a, n| { n => a } } if keys.present?
            # keys << curr_dir
            # keys.uniq!
            

            fld[rootdirect] = fol

            CodeReview.get_list_dir(rootdirect + "#{rootdirect.last == '/' ? '' : '/'}" + f.to_s, fld, keys, f, (hash_root.nil? ? curr_dir : hash_root))
          end
          # end
        end
      end
    fld
  end
  
  def self.extract_zip(file, destination, zip_file)
    FileUtils.mkdir_p(destination) if not File.directory?(destination)
    Zip::File.open(file) do |zip_file|
      zip_file.each do |f|
        fpath = File.join(destination, f.name)
        zip_file.extract(f, fpath) unless File.exist?(fpath)
      end
    end
    File.delete(zip_file)
  end

end
#count_lines_in_method("/home/amol/gitlab_project/voxpop/app/controllers/discount_coupons_controller.rb")

# sub_dir = ['app/models']
# root = "/home/amol/gitlab_project/shobiz/"

# send_root_and_sub_dir(root, sub_dir)





